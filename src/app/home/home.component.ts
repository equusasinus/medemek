import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  searchForm: FormGroup;
  submitted = false;
  phonePattern = /^(\+213|00213|0)(5|6|7)\d{8,8}$/gm;

  wilayas = [
    "Adrar",
    "Chlef",
    "Laghouat",
    "Oum el bouaghi",
    "Batna",
    "Bejaia",
    "Biskra",
    "Bechar",
    "Blida",
    "Bouira",
    "Tamanrasset",
    "Tebessa",
    "Tlemcen",
    "Tiaret",
    "Tizi-ouzou",
    "Alger",
    "Djelfa",
    "Jijel",
    "Sétif",
    "Saida",
    "Skikda",
    "Sidi Bel Abbes",
    "Annaba",
    "Guelma",
    "Constantine",
    "Médéa",
    "Mostaganem",
    "M'sila",
    "Mascara",
    "Ouargla",
    "Oran",
    "Bayedh",
    "Illizi",
    "Bordj Bou Arreridj",
    "Boumerdes",
    "El Tarf",
    "Tindouf",
    "Tissemsilt",
    "El Oued",
    "Khenchela",
    "Souk Ahras",
    "Tipaza",
    "Mila",
    "Ain Defla",
    "Naama",
    "In Temouchent",
    "Ghardaia",
    "Relizane",
    "Timimoune",
    "Bordj Badji Mokhtar",
    "Ouled Djellal",
    "Beni Abbes",
    "In Salah",
    "In Guezzam",
    "Touggourt",
    "Djanet",
    "M'ghair",
    "El Meniaa"
  ];

  selectedWilaya = this.wilayas[5];

  typeDeDon = [
    "Don de sang total",
    "Don par aphérèse",
    "Don de plasma",
    "Don de plaquettes"
  ];

  selectedType = this.typeDeDon[3];

  groupe = [
    "A+",
    "A-",
    "AB+",
    "AB-",
    "B+",
    "B-",
    "O+",
    "O-"
  ];

  selectedGroupe = this.groupe[0];

  persons = [
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    },
    {
      name: 'toto',
      groupeSanguin: 'A+',
      wilaya: 'Djelfa',
      description: "The Shiba Inu is the smallest of the six original and distinct spitz breeds of dog from Japan. A small, agile dog that copes very well with mountainous terrain, the Shiba Inu was originally bred for hunting."
    }
  ]

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.searchForm = this.formBuilder.group({
      wilaya: [this.selectedWilaya, Validators.required],
      typeDon: [this.selectedType, Validators.required],
      groupeSanguin: [this.selectedGroupe, Validators.required],
      phone: ['', Validators.pattern(this.phonePattern)]
    });
  }

  search() {
    console.log("Valuers de l'utilisateur");
    console.log(this.searchForm.value);
  }

}
